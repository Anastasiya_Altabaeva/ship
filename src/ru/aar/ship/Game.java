package ru.aar.ship;
import java.util.Scanner;

public class Game {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int channel = 11;
        int counter = 0;
        Ship ship = new Ship();
        System.out.println("Введите координату, в которую вы бы хотели выстрелить. Она не должна быть меньше 0 и больше 11");
        while (!ship.coordinates.isEmpty()) {
            int shot = scanner.nextInt();
            if ((shot < 0) || (shot > channel)) {
                System.out.println("Введите такую коорддинату,  которая будет соответствовать длине канала");
                shot = scanner.nextInt();
            }
            ship.shots(shot);
            counter ++;

        }
        System.out.println("Количество ваших выстрелов было = " + counter);
    }
}
