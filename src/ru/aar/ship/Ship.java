package ru.aar.ship;

 import  java.util.ArrayList;


public class Ship {

    ArrayList<Integer> coordinates = new ArrayList<>();

    Ship() {
        int x = (int) (Math.random() * 9);
        coordinates.add(x);
        coordinates.add(x + 1);
        coordinates.add(x + 2);
    }

    @Override
    public String toString() {
        return "Ship{" +
                "coordinates=" + coordinates +
                '}';
    }

    void shots(int shot) {
        if (coordinates.contains(shot)) {
            System.out.println("Попал");
            coordinates.remove(coordinates.indexOf(shot));
        } else {
            System.out.println("Мимо");
        }
        if (coordinates.isEmpty()) {
            System.out.println("Потопил");
        }
    }

    }
